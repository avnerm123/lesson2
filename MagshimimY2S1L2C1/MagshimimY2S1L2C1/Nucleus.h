#pragma once

#include<iostream>
#include<string>

#define CODON 3

class Gene {
private:
	unsigned int _start;
	unsigned int _end;
	bool _on_complementary_dna_strand;
public:
	void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);
	//getters
	unsigned int get_start() const;
	unsigned int get_end() const;
	bool is_on_complementary_dna_strand() const;
	//setters
	void setStart(unsigned int start) ;
	void setEnd(unsigned int end) ;
	void setComplementartDnaStrand(bool on_complementart_dna_strand);
};


class Nucleus {
private:
	std::string _DNA_strand;
	std::string _complementary_DNA_strand;
public:
	/*
	initialize the Nucleus class, and put the dna sequance in the _DNA_strand obj and is complemantary DNA sequance to the _complementary_DNA_strand
	input: a DNA sequance 
	output: none
	*/
	void init(const std::string dna_sequence);
	/*
	get's a Gene and from that gene create a RNA from the _DNA_strand
	inputs: a Gene
	output: an RNA transcript
	*/
	std::string get_RNA_transcript(const Gene& gene) const;
	/*
	returns the reversed DNA sequance
	input: none
	output: the reversed DNA sequance
	*/
	std::string get_reversed_DNA_strand()const;
	/*
	returns the number of times a codon apears in the DNA strand
	input: a codon: 3 letter's string
	output: the number of times that codon appears in the DNA strand
	*/
	unsigned int get_num_of_codon_appearances(const std::string& codon) const;
};


