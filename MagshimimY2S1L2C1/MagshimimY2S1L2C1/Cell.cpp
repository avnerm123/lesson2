#include "Cell.h"



void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	this->_mitochondrion.init();
	this->_nucleus.init(dna_sequence);
	this->_glocuse_receptor_gene = glucose_receptor_gene;

}

bool Cell::get_ATP()
{
	
	std::string RNA = this->_nucleus.get_RNA_transcript(this->_glocuse_receptor_gene);
	Protein* a = new Protein;
	a->init();
	a = this->_ribosome.create_protein(RNA);
	if (a->get_first()->get_data() == UNKNOWN)
	{
		std::cerr << "Error couldn't make the protein";
		_exit(01);
	}
	this->_mitochondrion.set_glucose(MIN + 1);
	this->_mitochondrion.insert_glucose_receptor(*a);
	
	if (this->_mitochondrion.produceATP())
	{
		this->_atp_units = 100;
		return true;
	}
	return false;
}
