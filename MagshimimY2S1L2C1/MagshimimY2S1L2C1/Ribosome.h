#pragma once
#include "Protein.h"


class Ribosome {
public:
	/*
	Creates a linked list of the Amino acids from the RNA transcript
	input: an RNA transcript
	output: a linked list of all the amino acisds from the RNA transcript
	*/
	Protein* create_protein(std::string& RNA_transcript) const;
};
