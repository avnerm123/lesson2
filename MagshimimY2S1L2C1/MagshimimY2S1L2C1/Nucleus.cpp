#include "Nucleus.h"

//Gene funcs

void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	this->_start = start;
	this->_end = end;
	this->_on_complementary_dna_strand = on_complementary_dna_strand;
}

unsigned int Gene::get_end() const
{
	return this->_end;
}

unsigned int Gene::get_start() const
{
	return this->_start;
}

bool Gene::is_on_complementary_dna_strand() const
{
	return this->_on_complementary_dna_strand;
}

void Gene::setComplementartDnaStrand(bool on_complementart_dna_strand)
{
	this->_on_complementary_dna_strand = on_complementart_dna_strand;
}

void Gene::setEnd(unsigned int end)
{
	this->_end = end;
}

void Gene::setStart(unsigned int start)
{
	this->_start = start;
}


//Nucleus funcs

void Nucleus::init(std::string dna_sequence)
{
	this->_DNA_strand = dna_sequence;


	std::string comp_DNA_sequance = "";

	for (int i = 0; i < dna_sequence.length(); i++)
	{
		if (dna_sequence[i] != 'A' && dna_sequence[i] != 'G' && dna_sequence[i] != 'C' && dna_sequence[i] != 'T')
		{
			std::cerr << "Error a DNA seq can't include a letter diffrate then A, C, G or T!";
			exit(1);
		}
		if (dna_sequence[i] == 'G')
		{
			comp_DNA_sequance += 'C';
		}
		else if (dna_sequence[i] == 'C')
		{
			comp_DNA_sequance += 'G';
		}
		else if (dna_sequence[i] == 'T')
		{
			comp_DNA_sequance += 'A';
		}
		else
		{
			comp_DNA_sequance += 'T';
		}
	}


	this->_complementary_DNA_strand = comp_DNA_sequance;
}

std::string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	std::string RNA_TRANS = "";

	for (int i = gene.get_start(); i < gene.get_end(); i++)
	{
		if (this->_DNA_strand[i] == 'T')
		{
			RNA_TRANS += 'U';
		}
		else
		{
			RNA_TRANS += this->_DNA_strand[i];
		}
	}

	return RNA_TRANS;
}

std::string Nucleus::get_reversed_DNA_strand() const
{
	std::string reversed_str;
	for (int i = this->_DNA_strand.length() - 1; i >= 0; i--)
		reversed_str += this->_DNA_strand[i];

	return reversed_str;
}

unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{
	std::string Nucleus_codon = "";
	unsigned int count = 0, count1 = 0 , j;

	for (int i = 0; i < this->_DNA_strand.length();i++)
	{
		j = 0;
		count = 0;
		while ((this->_DNA_strand[i] == codon[j]))
		{
			count++;
			i++;
			j++;
		}
		if (count == CODON)
		{
			count1++;
			count = 0;
		}
		else
			i++;
	}
	return count1;
}