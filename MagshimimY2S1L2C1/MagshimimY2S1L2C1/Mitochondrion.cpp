#include "Mitochondrion.h"

void Mitochondrion::init()
{
	this->_glocuse_level = 0;
	this->_has_glocuse_receptor = false;
}

void Mitochondrion::insert_glucose_receptor(const Protein& protein)
{
	bool is_glucose_receptor = true;
	int count = 0;
	AminoAcidNode* Node = protein.get_first();
	while (Node->get_next())
	{
		if (Node->get_data() != ALANINE && count == 0)
		{
			is_glucose_receptor = false;
		}
		if (Node->get_data() != LEUCINE && count == 1)
		{
			is_glucose_receptor = false;
		}
		if (Node->get_data() != GLYCINE && count == 2)
		{
			is_glucose_receptor = false;
		}
		if (Node->get_data() != HISTIDINE && count == 3)
		{
			is_glucose_receptor = false;
		}
		if (Node->get_data() != LEUCINE && count == 4)
		{
			is_glucose_receptor = false;
		}
		if (Node->get_data() != PHENYLALANINE && count == 5)
		{
			is_glucose_receptor = false;
		}
		if (Node->get_data() != AMINO_CHAIN_END && count == 6)
		{
			is_glucose_receptor = false;
		}
		Node = Node->get_next();
		count++;

	}
	this->_has_glocuse_receptor = is_glucose_receptor;
	
}


void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
	this->_glocuse_level = glocuse_units;
}

bool Mitochondrion::produceATP() const
{
	return this->_has_glocuse_receptor && this->_glocuse_level >= MIN;
}