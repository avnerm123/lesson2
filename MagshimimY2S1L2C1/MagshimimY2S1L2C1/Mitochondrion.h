#pragma once
#include "Protein.h"

#define MIN 50

class Mitochondrion {
private:
	unsigned int _glocuse_level;
	bool _has_glocuse_receptor;
public:
	/*
	Function's initialize the mitochondrion class, glocuse level turns to 0 and the bool  to false
	input: none
	output: none
	*/
	void init();
	/*
	functions set the boolean _has_glocuse_receptor if the linked list is: ALANINE->LEUCINE->GLYCINE->HISTIDINE->LEUCINE->PHENYLALANINE->AMINO_CHAIN_END
	input: a linked list of all the Amino acid's
	output: none
	*/
	void insert_glucose_receptor(const Protein& protein);
	/*
	Setter
	input: glocuse level
	output: none
	*/
	void set_glucose(const unsigned int glocuse_units);
	/*
	check if the glocuse level is more then 50 and the _hes_glocuse_receptor is true
	input: none
	output: if the cell can produce ATP
	*/
	bool produceATP() const;
};