#pragma once

#include "Mitochondrion.h"
#include "Nucleus.h"
#include "Ribosome.h"


class Cell {
private:
	Nucleus _nucleus;
	Ribosome _ribosome;
	Mitochondrion _mitochondrion;
	Gene _glocuse_receptor_gene;
	unsigned int _atp_units;
public:
	/*
	this function initialize the cell: the nucleus, the ribosome and the mitochondrion
	input: a dna sequance for the nucleus and a Gene for the _glocuse_receptor_gene
	output: none
	*/
	void init(const std::string dna_sequence, const Gene glucose_receptor_gene);
	/*
	creates an RNA from the Gene of the cell and from the DNA strand of the nucleus and then creates a linked list of aminoAcid's from the RNA,
	then sent the linked list to the mitochodrion to get if the amino acids are valid for ATP production if they are returns true else false
	input: none
	output: true if you can produce ATP and false if not
	*/
	bool get_ATP();

};